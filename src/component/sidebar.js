import React, {Component} from 'react';
import {sidebar} from '../css/sidebar.css';
import { Link } from 'react-router-dom';

export const Sidebar = () => {
    return (
        <div id="sidebar" className="sidebar-app">

            <ul className="navigation nav">
                <h2 className="title-section">Threejs shaders</h2>
                <li className="nav__item"> <Link to="1"> GLSL mouse time </Link> </li>
                <li className="nav__item"> <Link to="2"> use varying GLSL </Link> </li>
                <li className="nav__item">  <Link to="3"> GLSL light shaders </Link> </li>
                <li className="nav__item">  <Link to="4"> Vertex displacement I </Link> </li>
                <li className="nav__item">  <Link to="5"> Vertex displacement II </Link> </li>
                <li className="nav__item">  <Link to="6"> Vertex displacement III </Link> </li>
              <li className="nav__item">  <Link to="7"> Obj loader texture </Link> </li>
              <li className="nav__item">  <Link to="8"> Obj loader texture GLSL </Link> </li>
              <li className="nav__item">  <Link to="9"> Editor GLSL </Link> </li>
              <li className="nav__item">  <Link to="10"> Fragment shaders I </Link> </li>
              <li className="nav__item">  <Link to="11"> Fragment shaders II </Link> </li>
              <li className="nav__item">  <Link to="14"> 14 </Link> </li>
            </ul>

        </div>
    )
};