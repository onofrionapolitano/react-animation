import React, { Component } from 'react';
import * as THREE from 'three';
import Proptypes from 'prop-types';
class GeneralManager extends Component {
    scene = new THREE.Scene();
    renderer = new THREE.WebGLRenderer();
    constructor(props){
        super();
    }

    getChildContext(){
        return {
            scene: this.scene,
            renderer: this.renderer
        }
    }


    updateThree(props) {
        const {width, height} = props;
        this.renderer.setSize(width, height);
        this.renderer.setClearColor( "black", 1);
    }

    componentDidUpdate(){
        this.updateThree(this.props);
    }

    componentDidMount(){
        this.updateThree(this.props);
        console.log(this.props.children);
        this.refs.anchor.appendChild( this.renderer.domElement );

    }
    render(){
        return(
            <div>
                {this.props.children}
                <div className="App" ref="anchor"></div>
            </div>
        )
    }

}
GeneralManager.childContextTypes = {
    scene: Proptypes.object,
    renderer: Proptypes.object
};

export default GeneralManager;