import React, { Component } from  'react';
import * as THREE from 'three';
import Proptypes from 'prop-types';

class MaterialGeometry extends Component {
    constructor(props){
        super();
        console.log('props',props)
    }

    componentDidUpdate(){
// effettuo aggiornamento mesh dopo aver eliminato
     //   console.log('test aggiornamento vertex',this.props.vertex);
        this.material = new THREE.ShaderMaterial( {
            uniforms: this.uniforms,
            vertexShader:this.props.vertex,
            fragmentShader:this.props.fragment
        } );
        this.context.scene.remove( this.cube );

        this.shaderActive = this.props.shaderActive;
        (this.shaderActive == true) ? this.materialMesh = this.material : this.materialMesh = this.materialAlternative;
        this.cube = new THREE.Mesh( this.geometry, this.materialMesh );

        this.context.scene.add( this.cube );
    }

    componentDidMount(){
            this.uniforms = {
            u_time: { type: "f", value: 1.0 },
            u_resolution: { type: "v2", value: new THREE.Vector2() },
            u_mouse: { type: "v2", value: new THREE.Vector2() },
            lightPosition: {type: "v3", value: new THREE.Vector3()}
        };

        this.uniforms.u_resolution.value.x = this.context.renderer.domElement.width;
        this.uniforms.u_resolution.value.y = this.context.renderer.domElement.height;

      this.uniforms.u_mouse.value.x = 250.0;
      this.uniforms.u_mouse.value.y = 250.0;



     /* document.addEventListener('ready', (e) => {
        this.uniforms.u_mouse.value.x = (e.pageX - window.innerWidth / 2)/2 || 250;
        console.log( 'ini',this.uniforms.u_mouse.value.x)
        this.uniforms.u_mouse.value.y = (e.pageY - window.innerHeight / 2)/2 || 250;
      });
      */

      document.addEventListener('mousemove', (e) => {
        this.uniforms.u_mouse.value.x = (e.pageX - window.innerWidth / 2)/2 || 250;
        this.uniforms.u_mouse.value.y = (e.pageY - window.innerHeight / 2)/2 || 250;
      });




       this.geometry = new THREE.BoxGeometry( 1, 1, 1 );
    //  this.geometry = new THREE.SphereGeometry(3, 20, 20, 0, Math.PI * 2, 0, Math.PI * 2);

      console.log(this.geometrydue);

        this.material = new THREE.ShaderMaterial( {
            uniforms: this.uniforms,
            vertexShader:this.props.vertex,
            fragmentShader:this.props.fragment
        } );
        this.material.extensions.derivatives = true;

        this.materialAlternative = new THREE.MeshLambertMaterial( { color: 0xffffff, overdraw: 0.5 } );

        this.shaderActive = this.props.shaderActive;
        (this.shaderActive == true) ? this.materialMesh = this.material : this.materialMesh = this.materialAlternative;

        this.cube = new THREE.Mesh( this.geometry, this.materialMesh );

        this.context.scene.add( this.cube );


        this.animate();

    }

    animate = () => {
        requestAnimationFrame( this.animate );
        this.cube.rotation.x += 0.03;
        this.cube.rotation.y += 0.03;
        this.uniforms.u_time.value  += 0.1;
    };





    render(){
        return(
            <div>{this.props.children}</div>
        )
    }
}

MaterialGeometry.contextTypes = {
    scene: Proptypes.object,
    renderer: Proptypes.object
};



export default MaterialGeometry;