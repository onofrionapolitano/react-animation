import React, { Component } from 'react';
import * as THREE from 'three';
export class Esempio_4 extends Component {

    constructor(props){
        super();


    }


    componentDidMount(){
        const {width, height} = this.props;
        const scene = new THREE.Scene();
        const camera = new THREE.PerspectiveCamera( 45, window.innerWidth/window.innerHeight, 0.1, 1000 );

        const renderer = new THREE.WebGLRenderer();
        renderer.setSize(document.getElementById('test').clientWidth, height);
        this.refs.anchor.appendChild( renderer.domElement );




        var uniforms = {
            u_time: { type: "f", value: 1.0 },
            u_resolution: { type: "v2", value: new THREE.Vector2() },
            u_mouse: { type: "v2", value: new THREE.Vector2() },
            lightPosition: {type: "v3", value: new THREE.Vector3()}
        };

        uniforms.u_resolution.value.x = renderer.domElement.width;
        uniforms.u_resolution.value.y = renderer.domElement.height;


        var geometry = new THREE.SphereGeometry(3, 50, 50, 0, Math.PI * 2, 0, Math.PI * 2);
        // var geometry = new THREE.IcosahedronGeometry(10,4);
        var material = new THREE.ShaderMaterial( {
            uniforms: uniforms,
            vertexShader: `
             
// Include the Ashima code here!
  
varying vec2 vUv;
varying float noise;
uniform float u_time;
varying vec2 u_resolution;
varying vec2 u_mouse;

   float random (vec2 st) {
    return fract(sin(dot(st.xy, vec2(12.9898,78.233)))* 43758.5453123);
}

void main() {

    vUv = uv;
   //float randomNumber = step(pow(uv.y, 4.0) * random(uv), 0.2);
   float randomNumber = step(random(uv), 0.2);
   randomNumber = random(vec2(randomNumber * sin(u_time)));
    noise = pow(u_time * u_resolution.x, 2.0) ;
    float displacement = 0.3 * noise * randomNumber * u_mouse.x; 

    vec3 newPosition = position + (normal * randomNumber / uv.y * u_time); 
    gl_Position = projectionMatrix * modelViewMatrix * vec4( newPosition, 1.0 );

}
`,
            fragmentShader: `
            
varying vec2 u_resolution;            
varying vec2 vUv;
varying float noise;


void main() {

    vec3 color = vec3( vUv * ( 1. - 3.0 * u_resolution.y ), 1.0 );
   
    gl_FragColor = vec4( color.r,color.g, 0.4, 1.0 );

}`
        } );
        material.extensions.derivatives = true;
        var sphere = new THREE.Mesh( geometry, material );
        scene.add( sphere );

        camera.position.z = 30;


        var animate = function () {
            requestAnimationFrame( animate );
            uniforms.u_time.value += 0.01;
            sphere.rotation.x += 0.01;
            sphere.rotation.y += 0.01;

            renderer.render(scene, camera);

            document.onmousemove = function(e){
                uniforms.u_mouse.value.x = e.pageX
                uniforms.u_mouse.value.y = e.pageY
            }
        };
        animate();
    }
    render(){
        return(
            <div>



                <div className="App" ref="anchor"></div>
            </div>
        )
    }

}