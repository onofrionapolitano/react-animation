import React, { Component } from 'react';
import * as THREE from 'three';
import * as OBJLoader from 'three-obj-loader';
OBJLoader(THREE);
export class Esempio_8 extends Component {

    constructor(props){
        super(props);
    }


    componentDidMount(){
        this.THREE = THREE;
        const {width, height} = this.props;

        const camera = new THREE.PerspectiveCamera( 45, window.innerWidth/window.innerHeight, 0.1, 1000 );
        camera.position.z = 250;
        const scene = new THREE.Scene();
        const renderer = new THREE.WebGLRenderer({alpha: true});
        //  renderer.setClearColor( "#0000ef", 1 );
        renderer.setClearColor( "black", 1);

        renderer.setSize(document.getElementById('test').clientWidth, height);
        this.refs.anchor.appendChild( renderer.domElement );

        this.ambientLight = new THREE.AmbientLight( 'black', 0.4 );
        scene.add( this.ambientLight );
        this.pointLight = new THREE.PointLight( 'blue', 0.8 );
         camera.add( this.pointLight );
        scene.add( camera );


        var uniforms = {
            u_time: { type: "f", value: 1.0 },
            u_resolution: { type: "v2", value: new THREE.Vector2() },
            u_mouse: { type: "v2", value: new THREE.Vector2() },
            lightPosition: {type: "v3", value: new THREE.Vector3()},
            texture1: { type: "t", value: THREE.ImageUtils.loadTexture( "file/texture/texture.jpg" ) }
        };

        uniforms.u_resolution.value.x = renderer.domElement.width;
        uniforms.u_resolution.value.y = renderer.domElement.height;
        document.addEventListener('ready', function (e) {
            uniforms.u_mouse.value.x = (e.pageX - window.innerWidth / 2)/2;
            uniforms.u_mouse.value.y = (e.pageY - window.innerHeight / 2)/2;
        });

        document.addEventListener('mousemove', function (e) {
            uniforms.u_mouse.value.x = (e.pageX - window.innerWidth / 2)/2;
            uniforms.u_mouse.value.y = (e.pageY - window.innerHeight / 2)/2;
        });




        var material = new THREE.ShaderMaterial( {
            uniforms: uniforms,
            light: true,
            //  wireframe: true,
            vertexShader: `
     
            varying vec2 vUv;
varying float noise;
uniform float u_time;
varying vec2 u_resolution;
uniform vec2 u_mouse;
varying float Vmouse_pos;
varying vec3 newPosition;
varying vec3 vNormal;


   float random (vec2 st) {
    return fract(sin(dot(st.xy, vec2(12.9898,78.233)))* 43758.5453123);
}

void main() {
  vNormal = normal;  
  vUv = uv;
  Vmouse_pos = u_mouse.y;
if (u_mouse.x >= position.x && u_mouse.x <= u_mouse.x +100.0) {
    
    newPosition = vec3(position.x * (u_mouse.x/2000.0));
    newPosition = vec3(newPosition * position.y * u_mouse.y/2000.0);
    newPosition = vec3(newPosition * position.z * normalize(random(vec2(position.x, position.z))));
    
   
   }
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position - newPosition, 1.0);
    

}

`,
            fragmentShader: `
uniform sampler2D texture1;          
varying vec2 u_resolution;            
varying vec2 vUv;
varying float noise;
varying float Vmouse_pos;
varying vec3 vNormal;


float test(float i){
return normalize(i);
}

   float random (vec2 st) {
    return fract(tan(dot(st.xy, vec2(12.9898,78.233)))* 43758.5453123);
}
   float randomDue (vec2 st) {
    return floor(tan(dot(st.xy, vec2(12.9898,78.233)))* 43758.5453123);
}
 

void main() {

vec2 Tile = vec2(0.2, 0.2);
 vec3 light = vec3(0.5, 0.8 , 0.6);
 vec2 phase = fract(vUv / Tile); // creazione pattern 

           
  light = normalize(light);

  // calculate the dot product of
  // the light to the vertex normal
  float dProd = max(0.0, dot(vNormal, light)); 
   
   gl_FragColor = texture2D(texture1, phase) * dProd;
   // gl_FragColor = vec4( randomDue(u_resolution),0.1*dProd,test(vUv.x)*dProd, 1.0 );

}

`
        } );
     //   material.extensions.derivatives = true;


        var onProgress = function ( xhr ) {
            console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
        };

        var onError = function ( xhr ) {
            console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );
        };


        var manager = new THREE.LoadingManager();
        manager.onProgress = function ( item, loaded, total ) {
            console.log( item, loaded, total );
        };


        var textureLoader = new THREE.TextureLoader(manager);

        var texture = textureLoader.load('file/texture/steel_2.jpg');

        var loader = new this.THREE.OBJLoader(manager);
        loader.load('file/Shark.obj', function (object) {
                object.position.y -= 40;
                object.scale.x = 15;
                object.scale.y = 15;
                object.scale.z = 15;
                object.traverse( function ( child ) {

                    if ( child instanceof THREE.Mesh ) {
                        child.material.map = texture;
                         child.material = material;
                        console.log('texture', texture);



                    }

                } );

                scene.add(object);
            },onProgress, onError);




        var render = function () {
           camera.position.x += ( uniforms.u_mouse.value.x - camera.position.x ) * .002;
            camera.position.y += ( - uniforms.u_mouse.value.x - camera.position.y ) * .002;
            camera.lookAt( scene.position );
            uniforms.u_time.value += 0.01;
            renderer.render( scene, camera );
        };

        var animate = function () {

            requestAnimationFrame( animate );
            render();
        };
        animate();



    }





    render(){
        return(
            <div>



                <div className="App" ref="anchor"></div>
            </div>
        )
    }

}