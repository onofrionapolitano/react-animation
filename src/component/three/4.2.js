import React, {Component} from 'react';
import * as THREE from 'three';

export class Esempio_4 extends Component {

    constructor(props) {
        super();


    }


    componentDidMount() {
        const {width, height} = this.props;
        const scene = new THREE.Scene();
        const camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000);

        const renderer = new THREE.WebGLRenderer({alpha: true});
        //  renderer.setClearColor( "#0000ef", 1 );
        renderer.setClearColor("black", 1);

        renderer.setSize(document.getElementById('test').clientWidth, height);
        this.refs.anchor.appendChild(renderer.domElement);


        var uniforms = {
            u_time: {type: "f", value: 1.0},
            u_resolution: {type: "v2", value: new THREE.Vector2()},
            u_mouse: {type: "v2", value: new THREE.Vector2()},
            lightPosition: {type: "v3", value: new THREE.Vector3()}
        };

        uniforms.u_resolution.value.x = renderer.domElement.width;
        uniforms.u_resolution.value.y = renderer.domElement.height;
        document.addEventListener('ready', function (e) {
            uniforms.u_mouse.value.x = e.pageX;
            uniforms.u_mouse.value.y = e.pageY;
        });

        document.addEventListener('mousemove', function (e) {
            uniforms.u_mouse.value.x = e.pageX;
            uniforms.u_mouse.value.y = e.pageY;
        });


        var geometry = new THREE.SphereGeometry(3, 50, 50, 0, Math.PI * 2, 0, Math.PI * 2);
        // var geometry = new THREE.IcosahedronGeometry(10,4);
        var material = new THREE.ShaderMaterial({
            uniforms: uniforms,
            //  wireframe: true,
            vertexShader: `
            varying vec2 vUv;
varying float noise;
uniform float u_time;
varying vec2 u_resolution;
uniform vec2 u_mouse;
varying float Vmouse_pos;
varying vec3 newPosition;
varying vec3 vNormal;


   float random (vec2 st) {
    return fract(sin(dot(st.xy, vec2(12.9898,78.233)))* 43758.5453123);
}

void main() {
  vNormal = normal;  
  vUv = uv;
  Vmouse_pos = u_mouse.y;
    /* gl_Position =projectionMatrix * modelViewMatrix * vec4( position - random(vec2(position.z, normalize(position.x ))) * u_mouse.x/100.0, 1.0 ); */
    
    // gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
    
    newPosition = vec3(position.x * (u_mouse.x/2000.0));
    newPosition = vec3(newPosition * position.y * u_mouse.y/2000.0);
    newPosition = vec3(newPosition * position.z * normalize(random(vec2(position.x, position.z))));
    
    gl_Position = projectionMatrix * modelViewMatrix * vec4(position - newPosition, 1.0);
    

}

`,
            fragmentShader: `
           
varying vec2 u_resolution;            
varying vec2 vUv;
varying float noise;
varying float Vmouse_pos;
varying vec3 vNormal;


float test(float i){
return normalize(i);
}

   float random (vec2 st) {
    return fract(tan(dot(st.xy, vec2(12.9898,78.233)))* 43758.5453123);
}
   float randomDue (vec2 st) {
    return floor(tan(dot(st.xy, vec2(12.9898,78.233)))* 43758.5453123);
}
 

void main() {

 vec3 light = vec3(0.5, 0.2, 1.0);

           
  light = normalize(light);

  // calculate the dot product of
  // the light to the vertex normal
  float dProd = max(0.0, dot(vNormal, light)); 
   
    gl_FragColor = vec4( randomDue(u_resolution),0.1*dProd,test(vUv.x)*dProd, 1.0 );

}

`
        });
        material.extensions.derivatives = true;
        var sphere = new THREE.Mesh(geometry, material);
        scene.add(sphere);

        camera.position.z = 30;


        var animate = function () {
            requestAnimationFrame(animate);
            uniforms.u_time.value += 0.01;
            sphere.rotation.x += 0.01;
            sphere.rotation.y += 0.01;
            renderer.render(scene, camera);
        };
        animate();
    }

    render() {
        return (
            <div>


                <div className="App" ref="anchor"></div>
            </div>
        )
    }

}