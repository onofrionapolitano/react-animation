import React, { Component } from 'react';
import * as THREE from 'three';
import * as OBJLoader from 'three-obj-loader';
OBJLoader(THREE);
export class Esempio_7 extends Component {

    constructor(props){
        super(props);
    }


    componentDidMount(){
        this.THREE = THREE;
        const {width, height} = this.props;

        const camera = new THREE.PerspectiveCamera( 45, window.innerWidth/window.innerHeight, 0.1, 1000 );
        camera.position.z = 280;
        const scene = new THREE.Scene();
        const renderer = new THREE.WebGLRenderer({alpha: true});
        //  renderer.setClearColor( "#0000ef", 1 );
        renderer.setClearColor( "black", 1);

        renderer.setSize(document.getElementById('test').clientWidth, height);
        this.refs.anchor.appendChild( renderer.domElement );

        this.ambientLight = new THREE.AmbientLight( 'black', 0.4 );
        scene.add( this.ambientLight );
        this.pointLight = new THREE.PointLight( 'blue', 0.8 );
        camera.add( this.pointLight );
        scene.add( camera );


        var uniforms = {
            u_time: { type: "f", value: 1.0 },
            u_resolution: { type: "v2", value: new THREE.Vector2() },
            u_mouse: { type: "v2", value: new THREE.Vector2() },
            lightPosition: {type: "v3", value: new THREE.Vector3()}
        };

        uniforms.u_resolution.value.x = renderer.domElement.width;
        uniforms.u_resolution.value.y = renderer.domElement.height;
        document.addEventListener('ready', function (e) {
            uniforms.u_mouse.value.x = (e.pageX - window.innerWidth / 2)/2;
            uniforms.u_mouse.value.y = (e.pageY - window.innerHeight / 2)/2;
        });

        document.addEventListener('mousemove', function (e) {
            uniforms.u_mouse.value.x = (e.pageX - window.innerWidth / 2)/2;
            uniforms.u_mouse.value.y = (e.pageY - window.innerHeight / 2)/2;
        });


        var loader = new this.THREE.OBJLoader();

        loader.load('file/coca.OBJ', function (object) {
            object.position.y -= 100;
            scene.add(object);
        },
            function ( xhr ) {

                console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );

            },
            // called when loading has errors
            function ( error ) {

                console.log( 'An error happened' );

            }
            );
        
        var render = function () {
            camera.position.x += ( uniforms.u_mouse.value.x - camera.position.x ) * .05;
            camera.position.y += ( - uniforms.u_mouse.value.x - camera.position.y ) * .05;
            camera.lookAt( scene.position );
            uniforms.u_time.value += 0.01;
            //
            renderer.render( scene, camera );
        };

         var animate = function () {

            requestAnimationFrame( animate );
           render();
        };
        animate();



    }





    render(){
        return(
            <div>



                <div className="App" ref="anchor"></div>
            </div>
        )
    }

}