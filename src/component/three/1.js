import React, { Component } from 'react';
import * as THREE from 'three';
export class Esempio_1 extends Component {

    constructor(props){
        super();


    }


    componentDidMount(){
        const {width, height} = this.props;
        const scene = new THREE.Scene();
        const camera = new THREE.PerspectiveCamera( 45, window.innerWidth/window.innerHeight, 0.1, 1000 );

        const renderer = new THREE.WebGLRenderer();
        renderer.setSize(document.getElementById('test').clientWidth, height);
        this.refs.anchor.appendChild( renderer.domElement );




        var uniforms = {
            u_time: { type: "f", value: 1.0 },
            u_resolution: { type: "v2", value: new THREE.Vector2() },
            u_mouse: { type: "v2", value: new THREE.Vector2() },
            lightPosition: {type: "v3", value: new THREE.Vector3()}
        };

        uniforms.u_resolution.value.x = renderer.domElement.width;
        uniforms.u_resolution.value.y = renderer.domElement.height;



        var geometry = new THREE.BoxGeometry( 1, 1, 1 );
        var material = new THREE.ShaderMaterial( {
            uniforms: uniforms,
            vertexShader: `
			attribute vec3 center;
			varying vec3 vCenter;
			uniform float u_time;
			  uniform vec2 u_mouse;
			uniform mat4 uModelViewProjectionMatrix;
			void main() {
				vCenter = center;
				gl_Position = projectionMatrix * modelViewMatrix * vec4( position * sin(u_time)*u_mouse.x/600.0, 1.0 );
			}
`,
            fragmentShader: `
         
            uniform vec2 u_resolution;
            uniform float u_time;
            uniform vec3 lightPosition;
            
            float random (vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))*
        43758.5453123);
}

        void main() {
            vec2 st = gl_FragCoord.xy/u_resolution.xy;
            
            vec2 ipos = floor(st);
            vec2 fpos = fract(st);
            
       
            
            gl_FragColor=vec4(random(sin(fpos*u_time)), 0.5,sin(0.5*u_time),1.0);
        }`
        } );
        material.extensions.derivatives = true;
        var cube = new THREE.Mesh( geometry, material );
        scene.add( cube );


       console.log('test 2');




        camera.position.z = 2;


        var animate = function () {
            requestAnimationFrame( animate );
            uniforms.u_time.value += 0.01;
            cube.rotation.x += 0.01;
            cube.rotation.y += 0.01;

            renderer.render(scene, camera);

            document.onmousemove = function(e){
                uniforms.u_mouse.value.x = e.pageX;
                uniforms.u_mouse.value.y = e.pageY;
            }

        };
        animate();
    }
    render(){
        return(
            <div>



            <div className="App" ref="anchor"></div>
            </div>
        )
    }

}





