import React from 'react';
import * as THREE from 'three';

import ReactDOM from "react-dom";
import * as vertexShader from '../../shaders/vert1.vert';
import * as fragShader from '../../shaders/frag1.frag';
const measureElement = element => {
  const DOMNode = ReactDOM.findDOMNode(element);
  return {
    width: DOMNode.offsetWidth,
    height: DOMNode.offsetHeight,
  };
}

const convertPositionTHREE = (canvasW, canvasH, PosX, PosY, planeW, planeH, offsetContainerX) => {
  let convertW = planeW / 2 - canvasW / 2 + PosX - offsetContainerX;
  let convertH = -planeH / 2 + canvasH / 2 - PosY;
  return {
    width: convertW, height: convertH
  }
};



export class Esempio9 extends React.Component {
  constructor(props) {
    super(props);
    this.THREE = THREE;
  }

  OnLoadImage(e){

  e.target.setAttribute('data-loaded','true');
  //  console.log(this.target.getAttribute('data-loaded'));
  }


  // aggiungi plane al  inizio
  init() {
    let screen = document.querySelector('.container--img');
   // const SCREEN_WIDTH = window.innerWidth;
   const SCREEN_WIDTH = screen.clientWidth;
    const SCREEN_HEIGHT =  screen.clientHeight;
    this.camera = new THREE.PerspectiveCamera( 45, SCREEN_WIDTH/SCREEN_HEIGHT, 1, SCREEN_HEIGHT * 2 );
    let vFOV = this.camera.fov * (Math.PI / 180);
    this.camera.position.z = SCREEN_HEIGHT / (2 * Math.tan(vFOV / 2) );
    this.scene = new THREE.Scene();
    this.renderer = new THREE.WebGLRenderer({alpha: true});
    //  renderer.setClearColor( "#0000ef", 1 );
    this.renderer.setClearColor( "black", 1);

    this.renderer.setSize(SCREEN_WIDTH,SCREEN_HEIGHT);
    this.refs.anchor.appendChild( this.renderer.domElement );

    console.log('wx', this.renderer);

    this.ambientLight = new THREE.AmbientLight( 'black', 0.4 );
    this.scene.add( this.ambientLight );
    this.pointLight = new THREE.PointLight( 'blue', 0.8 );
    this.camera.add( this.pointLight );
    this.scene.add( this.camera );




    //plane canvas generator

    let imgSelect = document.querySelectorAll('.trd');
    this.multiMesh = [];
    imgSelect.forEach((el) => {


   //   if(el.getAttribute('data-loaded')===true) {
        let w = el.clientWidth;
        let h = el.clientHeight;
        let top = el.getBoundingClientRect().top + window.scrollY;
        let left = el.getBoundingClientRect().left + window.scrollX;
        let texture = el.src;
        let offsetContainerX = screen.getBoundingClientRect().left;

        console.log(left);


        let loaderTexture = new THREE.TextureLoader;
        loaderTexture.load(texture);

      this.uniforms = {
        uTime: { value: 0 },
        uRandom: { value: 1.0 },
        uDepth: { value: 2.0 },
        uSize: { value: 0.0 },
        uTextureSize: {
          value: new THREE.Vector2(w, h),
        },
        uTexture: { value: loaderTexture.load(texture) },
        uTouch: { value: null },
      };

      let frag = `
      precision highp float;
      uniform sampler2D uTexture;
 varying vec2 Puv;
   //varying vec2 Ptexture;
void main() {
 vec2 test = Puv;
   vec4 tex = texture2D ( uTexture, Puv );
   vec4 texBN =  vec4(0.5, 0.6, 0.5, 1.0);
  //  gl_FragColor = vec4(0.5, 0.6, 0.5, 1.0);
   gl_FragColor = mix(tex, texBN, tex*10000.0);
}
      `;
      let vert = `
      precision highp float;

attribute float pindex;
attribute vec3 position;
attribute vec3 offset;
attribute vec2 uv;
attribute float angle;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

uniform float uTime;
uniform float uRandom;
uniform float uDepth;
uniform float uSize;
uniform vec2 uTextureSize;
uniform sampler2D uTexture;
uniform sampler2D uTouch;

 varying vec2 Puv;
  // varying vec2 Ptexture;
void main(){
    // final position
   Puv = uv;
  // Ptexture = uTexture;
    gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
}
      `

     // const vertexShader = require('raw-loader!glslify-loader!../../shaders/vert1.vert');
     // const fragShader = require('raw-loader!glslify-loader!../../shaders/frag1.vert');

        this.materialShaders  = new THREE.RawShaderMaterial({
        uniforms: this.uniforms,
        vertexShader: vert,
        fragmentShader: frag,
        depthTest: false,
        transparent: true,
      //  blending: THREE.AdditiveBlending,
      });

        let THPosition = convertPositionTHREE(SCREEN_WIDTH, SCREEN_HEIGHT, left, top, w, h, offsetContainerX );
        this.material = new THREE.MeshPhongMaterial({
          // color: 0xffff00,
          map: loaderTexture.load(texture),
          // displacementMap: loaderTexture.load(texture),
          // emissive: 0xffff00
        });
        this.geometry = new THREE.PlaneBufferGeometry(w, h, 2, 2);

        // aggiungi plane al canvas fine




        this.plane = new THREE.Mesh(this.geometry, this.materialShaders);
        this.plane.position.set(THPosition.width, THPosition.height, 0);
        this.multiMesh.push(this.plane);

      this.scene.add(this.plane);



   //  }

    });

    console.log('scena',this.scene)

  };

   renderCanvas = () => {
     requestAnimationFrame( this.renderCanvas );
      if(this.renderer) {
        this.renderer.render(this.scene, this.camera);

      }
    };

   onResize = () => {
     /*this.multiMesh.forEach((el)=> {
      this.scene.remove(el);
      el.geometry.dispose();
     })*/
     let screen = document.querySelector('.container--img');
     // const SCREEN_WIDTH = window.innerWidth;
     const SCREEN_WIDTH = screen.clientWidth;
     const SCREEN_HEIGHT =  screen.clientHeight;
     this.camera.aspect = SCREEN_WIDTH/SCREEN_HEIGHT;
     this.camera.far = SCREEN_HEIGHT * 2;
     let vFOV = this.camera.fov * (Math.PI / 180);
     this.camera.position.z = SCREEN_HEIGHT / (2 * Math.tan(vFOV / 2) );
     this.renderer.setSize(SCREEN_WIDTH,SCREEN_HEIGHT);
     this.camera.updateProjectionMatrix();

     this.plane.geometry.dispose();
     console.log(this.scene);
     let imgSelect = document.querySelectorAll('.trd');
     imgSelect.forEach((el, index) => {

       let w = el.clientWidth;
       let h = el.clientHeight;
       let top = el.getBoundingClientRect().top + window.scrollY;
       let left = el.getBoundingClientRect().left + window.scrollX;
       let offsetContainerX = screen.getBoundingClientRect().left;
       let texture = el.src;
       let THPosition = convertPositionTHREE(SCREEN_WIDTH, SCREEN_HEIGHT, left, top, w, h, offsetContainerX );

       console.log(this.multiMesh[index].position, index);

      // this.multiMesh[index].set(THPosition.width, THPosition.height, 0);

       //this.plane.position.set(THPosition.width, THPosition.height, 0);
       this.multiMesh[index].position.set(THPosition.width, THPosition.height, 0);
       this.newGeometry = new THREE.PlaneBufferGeometry(w, h, 2, 2);
       this.multiMesh[index].geometry.dispose();
       this.multiMesh[index].geometry = this.newGeometry;


     });



   // console.log('ecco lo scren',screen.clientWidth);
   };



  componentDidMount() {
    window.addEventListener('resize',this.onResize.bind(this),false);


   setTimeout(()=>{this.init()},1000);
    this.renderCanvas();
  }
  componentWillUnmount() {
    console.log('tolto');
   // window.removeEventListener('load',this.init.bind(this),false);
  }

  render(){
    return (
    <div>

      <div className="container-fluid container--img">
        <div className="row">
          <div   className="col-6" style={{marginTop: '60px'}}>
        <img data-loaded={false} className="trd"  src="https://media.gettyimages.com/photos/portrait-of-a-bearded-man-picture-id1154505027?s=2048x2048"/>
          </div>

          <div className="col-6" style={{marginTop: '180px'}}>
            <img data-loaded={false} className="trd"  src="https://media.gettyimages.com/photos/plastic-containers-repurposed-as-vases-picture-id1159090704?s=2048x2048"/>
          </div>


          <div className="col-6" style={{marginTop: '40px'}}>
            <img data-loaded={false} className="trd"  src="https://media.gettyimages.com/vectors/vertical-abstract-triangles-geometric-background-green-blue-white-vector-id831840550"/>
          </div>

          <div className="text col-6">
            <h1 style={{color: "white", fontSize:"70px"}}>Test</h1>
          </div>

          <div className="text col-6">
            <h1 style={{color: "white", fontSize:"70px"}}>Test</h1>
          </div>

          <div className="col-6" style={{marginTop: '190px'}}>
            <img data-loaded={false} onLoad={(e)=>this.OnLoadImage(e)}  className="trd"    src="https://media.gettyimages.com/photos/surprised-young-man-drinking-soda-while-watching-movie-with-friend-in-picture-id1146822780?s=2048x2048"/>
          </div>

        </div>

      </div>


      <div className="App" ref="anchor"></div>
    </div>
      )

  }
}

