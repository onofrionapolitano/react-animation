import React, { Component } from  'react';
import * as THREE from "three";
import Proptypes from 'prop-types';

class SceneLight extends Component {


    updateThree(props) {
        const {fov, aspect, near, far, ambient, ambientOpacity, point, pointOpacity} = this.props;
        this.camera = new THREE.PerspectiveCamera( fov, aspect,near, far );
        this.ambientLight = new THREE.AmbientLight( ambient, ambientOpacity );
        this.pointLight = new THREE.PointLight( point, pointOpacity );
        this.camera.position.z = 2;
    }

    constructor(props){
        super(props);
        this.updateThree(this.props);
    }


    componentDidUpdate() {
        this.updateThree(this.props);
    }


    componentDidMount(){
        //console.log(this.context);
        this.updateThree(this.props);
        this.camera.add( this.pointLight );
        this.context.scene.add( this.ambientLight );
        this.context.scene.add( this.camera );
        this.animate();
    }

    animate = () => {
        requestAnimationFrame( this.animate );
        this.camera.lookAt( this.context.scene.position );
        this.context.renderer.render(this.context.scene, this.camera);
    };



    render(){
        return(
            <div>{this.props.children}</div>
        )
    }
}
SceneLight.contextTypes = {
  scene: Proptypes.object,
    renderer: Proptypes.object
};
export default SceneLight;