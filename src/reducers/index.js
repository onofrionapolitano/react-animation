import { combineReducers } from 'redux';

import { pageReducer } from './reducer';

const rootReducer = combineReducers({
    page: pageReducer
});

export default rootReducer;
