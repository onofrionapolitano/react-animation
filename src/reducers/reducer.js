import { READ } from '../action/index';

export function pageReducer (state={}, action) {
    switch (action.type) {
       case READ: {
            return action.payload.data
        }

        default:  return state;
    }

}