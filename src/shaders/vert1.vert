precision highp float;

attribute float pindex;
attribute vec3 position;
attribute vec3 offset;
attribute vec2 uv;
attribute float angle;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

uniform float uTime;
uniform float uRandom;
uniform float uDepth;
uniform float uSize;
uniform vec2 uTextureSize;
uniform sampler2D uTexture;
uniform sampler2D uTouch;

void main(){
    // final position
  //  varying vec2 Puv = uv;
 //   varying vec2 Ptexture = Utexture;
    gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
}