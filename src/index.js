import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Helmet from 'react-helmet';
import { createStore, applyMiddleware } from 'redux';
import promise from 'redux-promise';
import { BrowserRouter, Route, Switch} from 'react-router-dom';
import './index.css';
import reducer from './reducers';

import {Sidebar} from './component/sidebar';
import App from './App';
import PrimaPagina from './container/prima_pagina';
import SecondaPagina from './container/seconda_pagina';
import TerzaPagina from './container/3_pagina';
import QuartaPagina from './container/4_pagina';
import QuintaPagina from './container/5_pagina';
import SestaPagina from './container/6_pagina';
import SettePagina from './container/7_pagina';
import OttoPagina from './container/8_pagina';
import NonaPagina from "./container/9_pagina";
import DieciPagina from './container/10_pagina';
import UndiciPagina from './container/11_pagina';
import DodiciPagina from './container/12_pagina';
import TrediciPagina from './container/13_pagina';
import QuattordiciPagina from "./container/14_pagina";
import registerServiceWorker from './registerServiceWorker';





const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

// uso hydrate invece di render per la questione SSR
// andrà testata la funzionalità



ReactDOM.hydrate(
   <Provider store={createStoreWithMiddleware(reducer)}>
<div className="container-app">



        <BrowserRouter>

            <div className="content-app">

              <Helmet>
                <meta property="fb:app_id" content="228688481161812" />
                <meta property="og:url" content="http://drib.tech/fbsharetest/quiz.html" />
                <meta property="og:type" content="article" />
                <meta property="og:title" content="Are you the real Star Trek geek?" />
                <meta property="og:description" content="Prove your Star Trek geekhood! Are you wise like Yoda or Jar Jar Binks?" />
                <meta property="og:image" content="http://drib.tech/fbsharetest/quiz_landing.jpg" />
                <meta property="og:image:width" content="1200" />
                <meta property="og:image:height" content="630" />
              </Helmet>

                <Sidebar/>
                <Switch>
                  <Route path="/14" component={QuattordiciPagina}/>
                  <Route path="/13" component={TrediciPagina}/>
                  <Route path="/12" component={DodiciPagina}/>
                  <Route path="/11" component={UndiciPagina}/>
                  <Route path="/10" component={DieciPagina}/>
                    <Route path="/9" component={NonaPagina}/>
                    <Route path="/8" component={OttoPagina}/>
                    <Route path="/7" component={SettePagina}/>
                    <Route path="/6" component={SestaPagina}/>
                    <Route path="/5" component={QuintaPagina}/>
                    <Route path="/4" component={QuartaPagina}/>
                    <Route path="/3" component={TerzaPagina}/>
                    <Route path="/2" component={SecondaPagina}/>
                    <Route path="/1" component={PrimaPagina}/>
                    <Route path="/" component={App}/>
                </Switch>
            </div>
        </BrowserRouter>
</div>
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();
