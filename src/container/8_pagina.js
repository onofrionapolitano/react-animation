import React, { Component } from 'react';
import {Esempio_8} from "../component/three/8";

class OttoPagina extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <section>
                <div className="desc">

                    <p>vertex shader displacement | uso light shaders |  obj loader | texture </p>
                </div>
                <section  id="test" className="test" ref="anchor">
                    <Esempio_8 width={400} height={window.innerHeight} />
                </section>
            </section>
        )
    }

}

export default OttoPagina;