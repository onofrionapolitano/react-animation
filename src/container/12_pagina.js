import React, {Component} from 'react';
import GeneralManager from "../component/generalManager";
import SceneLight from "../component/sceneLight";
import MaterialGeometry from "../component/materialGeometry";

class DodiciPagina extends Component {

  perspective = {
    fov: 51,
    aspect: window.innerWidth / window.innerHeight,
    near: 0.1,
    far: 80
  };

  light = {
    ambient: 0x00020,
    ambientOpacity: 0.5,
    point: 0x0040ff,
    pointOpacity: 0.6
  };

  geometry = {
    cube: false,
    sphere: true
  };

  summary = { ...this.perspective, ...this.light };


  vertex = `attribute vec3 center;
			varying vec3 vCenter;
			uniform float u_time;
			uniform vec2 u_mouse;
			uniform mat4 uModelViewProjectionMatrix;
			
			  float random (vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))*
        43758.5453123);
}
			
			void main() {
			   vec3 newPosition = position * vec3(random(vec2(normalize(u_time) * position.x )));
			   newPosition = position - newPosition * vec3((random(uv)));
			   newPosition = newPosition + (position) * distance(uv.y, 0.5);
				vCenter = center;
				gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
			}`;
  fragment = `uniform vec2 u_resolution;
            uniform float u_time;
         




void main() {

vec3 color;
float test = 40.0;
    
   if(u_time < 40.0) {
    color = vec3(0.5*(u_time/20.0), 0.4, 0.3);
   } else if( u_time > 41.0 &&  u_time < 80){
   test  -= 0.03;
   color = vec3(0.5*(test/20.0), 0.4, 0.3);
   }
    gl_FragColor = vec4(color,1.0);
    }

        `;


  esegui() {
    this.setState({ vertex: this.vertex, fragment: this.fragment })
  }

  constructor(props) {
    super(props);
    this.esegui = this.esegui.bind(this);
    this.state = {
      width: null,
      shaderActive: true,
      vertex: this.vertex,
      fragment: this.fragment,
    }
  }

  componentDidMount() {
    this.sidebar = document.getElementById('sidebar').clientWidth;
    this.renderWidth = window.innerWidth - this.sidebar - 540; // 540 dimensione contenitore
    this.setState({ width: this.renderWidth });
  }

  render() {
    return (
      <section id="scene">
        <GeneralManager width={this.state.width} height={window.innerHeight}>
          <SceneLight {...this.summary}/>
          <MaterialGeometry shaderActive={this.state.shaderActive} vertex={this.state.vertex}
                            fragment={this.state.fragment}/>
        </GeneralManager>
      </section>
    )
  }
}

export default DodiciPagina;

