import React from 'react';
import GeneralManager from "../component/generalManager";
import SceneLight from "../component/sceneLight";
import MaterialGeometry from "../component/materialGeometry";
import "../css/9.css";

import brace from 'brace';
import AceEditor from 'react-ace';

import 'brace/mode/javascript';
import 'brace/theme/solarized_dark';


class NonaPagina extends React.Component {


    perspective = {
        fov: 45,
        aspect: window.innerWidth / window.innerHeight,
        near: 1,
        far: 40
    };

    light = {
        ambient: 0x00020,
        ambientOpacity: 0.5,
        point: 0x0040ff,
        pointOpacity: 0.6
    };

    summary = {...this.perspective, ...this.light};

    vertex = `attribute vec3 center;
			varying vec3 vCenter;
			uniform float u_time;
			uniform vec2 u_mouse;
			uniform mat4 uModelViewProjectionMatrix;
			void main() {
				vCenter = center;
				gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
			}`;
    fragment = `uniform vec2 u_resolution;
            uniform float u_time;
            uniform vec3 lightPosition;
            
            float random (vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))*
        43758.5453123);
}

        void main() {
            vec2 st = gl_FragCoord.xy/u_resolution.xy;
            
            vec2 ipos = floor(st);
            vec2 fpos = fract(st);
            
       
            
            gl_FragColor=vec4(random(sin(fpos*u_time)), 0.5,sin(0.5*u_time),1.0);
        }`;

    onChangeVertex(newValue) {
        //  this.setState({vertex:newValue})
        // return newValue;
        this.vertex = newValue;
    }

    onChangeFragment(newValue) {
        //  this.setState({fragment:newValue})
        // return newValue;
        this.fragment = newValue;
    }

    // salvo il cambiamento del fragment e shaders nelle variabili vertex e fragment
    // eseguo il nuovo stato assegnando i nuovi vertex e fragment al click sul pulsante

    esegui() {
        this.setState({vertex: this.vertex, fragment: this.fragment})
    }

    constructor(props) {
        super(props);
        this.selectMaterial = this.selectMaterial.bind(this);
        this.onChangeVertex = this.onChangeVertex.bind(this);
        this.onChangeFragment = this.onChangeFragment.bind(this);
        this.esegui = this.esegui.bind(this);
        this.state = {
            width: null,
            shaderActive: false,
            vertex: this.vertex,
            fragment: this.fragment,
        }
    }

    componentDidMount() {
        this.sidebar = document.getElementById('sidebar').clientWidth;
        this.renderWidth = window.innerWidth - this.sidebar - 540; // 540 dimensione contenitore
        this.setState({width: this.renderWidth});
    }


    selectMaterial() {
        (this.state.shaderActive == false) ? this.setState({shaderActive: true}) : this.setState({shaderActive: false});
        console.log(this.state.shaderActive);
    }


    render() {

        return (
            <section id="scene">
                <div className="editor-glsl">
                    {(this.state.shaderActive == true) ?
                        <section>
                          <h1>Vertex shader</h1>
                            <AceEditor
                                mode="javascript"
                                theme="solarized_dark"
                                onChange={this.onChangeVertex}
                                value={this.state.vertex}
                                name="vertex-editor"
                                editorProps={{$blockScrolling: true}}
                            />
                          <h1>Fragment shader</h1>
                            <AceEditor
                                mode="javascript"
                                theme="solarized_dark"
                                onChange={this.onChangeFragment}
                                value={this.state.fragment}
                                name="fragment-editor"
                                editorProps={{$blockScrolling: true}}
                            />

                            <button onClick={this.esegui}>Esegui shaders</button>
                        </section>
                        : null}

                </div>
                <div className="render-element">

                    <div className="select-material">
                        <button onClick={this.selectMaterial}>
                            {(this.state.shaderActive == true) ?

                                `abilita threejs mesh`
                                :

                                `abilita modalità shaders`

                            }
                        </button>
                    </div>

                    <GeneralManager width={this.state.width} height={window.innerHeight}>
                        <SceneLight {...this.summary}/>
                        <MaterialGeometry shaderActive={this.state.shaderActive} vertex={this.state.vertex}
                                          fragment={this.state.fragment}/>
                    </GeneralManager>
                </div>
            </section>
        )
    }

}


export default NonaPagina;