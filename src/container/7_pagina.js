import React, { Component } from 'react';
import {Esempio_7} from "../component/three/7";

class SettePagina extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <section>
                <div className="desc">

                    <p>vertex shader displacement | uso light shaders | u_time displacement </p>
                </div>
                <section  id="test" className="test" ref="anchor">
                    <Esempio_7 width={400} height={window.innerHeight} />
                </section>
            </section>
        )
    }
}

export default SettePagina;