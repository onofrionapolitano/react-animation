import React, { Component } from 'react';
import GeneralManager from "../component/generalManager";
import SceneLight from "../component/sceneLight";
import MaterialGeometry from "../component/materialGeometry";

class DieciPagina extends Component {

  perspective = {
    fov: 51,
    aspect: window.innerWidth / window.innerHeight,
    near: 0.1,
    far: 80
  };

  light = {
    ambient: 0x00020,
    ambientOpacity: 0.5,
    point: 0x0040ff,
    pointOpacity: 0.6
  };

  summary = {...this.perspective, ...this.light};


  vertex = `attribute vec3 center;
			varying vec3 vCenter;
			uniform float u_time;
			uniform vec2 u_mouse;
			uniform mat4 uModelViewProjectionMatrix;
			void main() {
				vCenter = center;
				gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
			}`;
  fragment = `uniform vec2 u_resolution;
            uniform float u_time;
            uniform vec3 lightPosition;
            
            float random (vec2 st) {
    return fract(sin(dot(st.xy,
                         vec2(12.9898,78.233)))*
        43758.5453123);
        
}
       float colore (in vec2 fpos, in vec2 ipos, in float u_time, in vec2 st) {
       return step(fpos.x, 0.2) +
            step(fpos.y, 0.9 * tan(u_time)) 
            * random(st * vec2(u_time)) 
            * tan(u_time/st.x) 
            - smoothstep(random(vec2(u_time)), 0.1, 0.2)
            + sin(step(st.x, 0.5))
            * length( abs(st)*.3);
          
       }
     
        void main() {
            vec2 st = gl_FragCoord.xy/u_resolution.xy;
            
            vec2 ipos = floor(st);
            vec2 fpos = fract(st);
            
            float pct = distance(st,vec2(0.5));
       
        
        
  
       
           gl_FragColor=vec4(
            vec2(colore (fpos,ipos,u_time,st))
            ,sin(colore(fpos,ipos,u_time,st)), 1.0);
       
            
            
           
      
        }`;



  esegui() {
    this.setState({vertex: this.vertex, fragment: this.fragment})
  }

  constructor(props) {
    super(props);
    this.esegui = this.esegui.bind(this);
    this.state = {
      width: null,
      shaderActive: true,
      vertex: this.vertex,
      fragment: this.fragment,
    }
  }

  componentDidMount() {
    this.sidebar = document.getElementById('sidebar').clientWidth;
    this.renderWidth = window.innerWidth - this.sidebar - 540; // 540 dimensione contenitore
    this.setState({width: this.renderWidth});
  }

  render(){
    return(
   <section id="scene">
     <div className="desc">

       <p>Fragment shaders | time | noise random generator </p>
     </div>
     <GeneralManager width={this.state.width} height={window.innerHeight}>
       <SceneLight {...this.summary}/>
       <MaterialGeometry shaderActive={this.state.shaderActive} vertex={this.state.vertex} fragment={this.state.fragment} />
     </GeneralManager>
   </section>
    )
  }

}

export default DieciPagina;