import React, {Component} from 'react';
import {Esempio9} from '../component/three/9';
import   '../css/14.css';
import '../css/bootstrap.css';

class QuattordiciPagina extends Component {
  constructor(props) {
    super(props);
  }
  render(){
    return (
      <section>
        <div className="desc">

          <p>uso multi texture generator</p>
        </div>
        <section  id="test" className="test" ref="anchor">
          <Esempio9 width={400} height={window.innerHeight} />
        </section>
      </section>
    )
  }
}

export default QuattordiciPagina;
