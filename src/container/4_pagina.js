import React, {Component} from 'react';
import {Esempio_4} from '../component/three/4.2';


class QuartaPagina extends Component {
    constructor(props){
        super(props);
        this.divStyle = {
            overflow: 'hidden'
        };
    }

    render(){
        return(
            <section>
                <div className="desc">

                    <p>vertex shader displacement | uso light shaders | random | mouse event</p>
                </div>
                <section  id="test" className="test" ref="anchor">
                    <Esempio_4 width={400} height={window.innerHeight} />
                </section>
            </section>
        )
    }
}

export default QuartaPagina;