import React, { Component } from 'react';
import {Esempio_1} from '../component/three/1';

class PrimaPagina extends Component {

    constructor(props) {
        super();

    }

    componentDidMount(){


        this.state = {
            element:  document.getElementById('test').clientWidth
        };
        console.log(this.state.element);

       // console.log();
    }


    render() {
        return (
               <section>
                   <div className="desc">

                       <p>background con noise e color generator
                       | mouse event</p>
                   </div>
                <section  id="test" className="test" ref="anchor">
                    <Esempio_1 width={400} height={window.innerHeight} />
                </section>
               </section>
          

        );
}
}

export default PrimaPagina;
