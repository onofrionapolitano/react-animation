import React, { Component } from 'react';
import {Esempio_2} from '../component/three/2';

class SecondaPagina extends Component {
   constructor(props) {
       super();
   }


    render(){
        return(
            <section>
                <div className="desc">

                    <p>uso di varying per il passaggio degli shaders da vertex ai fragment</p>
                </div>
            <section  id="test" className="test" ref="anchor">
                <Esempio_2 width={400} height={window.innerHeight} />
            </section>
            </section>
        )
    }

}

export default SecondaPagina;