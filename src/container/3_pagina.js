import React, {Component} from 'react';
import {Esempio_3} from '../component/three/3';

class TerzaPagina extends Component {
    constructor(props) {
        super(props);
    }
    render(){
        return (
            <section>
                <div className="desc">

                    <p>uso light shaders | gradient generator</p>
                </div>
                <section  id="test" className="test" ref="anchor">
                    <Esempio_3 width={400} height={window.innerHeight} />
                </section>
            </section>
        )
    }
}

export default TerzaPagina;