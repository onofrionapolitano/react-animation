import React, {Component} from 'react';
import GeneralManager from "../component/generalManager";
import SceneLight from "../component/sceneLight";
import MaterialGeometry from "../component/materialGeometry";

class TrediciPagina extends Component {



  perspective = {
    fov: 51,
    aspect: window.innerWidth / window.innerHeight,
    near: 0.1,
    far: 80
  };

  light = {
    ambient: 0x00020,
    ambientOpacity: 0.5,
    point: 0x0040ff,
    pointOpacity: 0.6
  };

  geometry = {
    cube: false,
    sphere: true
  };


  summary = { ...this.perspective, ...this.light };


  fragment = `
 uniform vec2 u_resolution;
 uniform vec2 u_mouse;
 uniform  float u_time;
  vec2 random2(vec2 st){
    st = vec2( dot(st,vec2(127.1,311.7)),
              dot(st,vec2(269.5,183.3)) );
    return -1.0 + 2.0*fract(sin(st)*43758.5453123);
}

// Value Noise by Inigo Quilez - iq/2013
// https://www.shadertoy.com/view/lsf3WH
float noise(vec2 st) {
    vec2 i = floor(st);
    vec2 f = fract(st);

    vec2 u = f*f*(3.0-2.0*f*f);

 /*   return mix( mix( dot( random2(i + vec2(0.0,0.0) ), f - vec2(0.0,0.0) ),
                     dot( random2(i + vec2(1.0,0.0) ), f - vec2(1.0,0.0) ), u.x),
                mix( dot( random2(i + vec2(0.0,1.0) ), f - vec2(0.0,1.0) ),
                     dot( random2(i + vec2(1.0,1.0) ), f - vec2(1.0,1.0) ), u.x), u.y); */
                     
     return mix( mix( dot( random2(i + vec2(0.0,0.0) ), f - vec2(0.0,0.0) ),
                     dot( random2(i + vec2(1.0,0.0) ), f - vec2(1.0,0.0) ), u.x),
                mix( dot( random2(i + vec2(0.0,1.0) ), f - vec2(0.0,1.0) ),
                     dot( random2(i + vec2(1.0,1.0) ), f - vec2(1.0,1.0) ), u.x), u.y);                 
}

void main() {
    vec2 st = gl_FragCoord.xy/u_resolution.xy;
    vec2 u_mouse_st = gl_FragCoord.xy/u_mouse.xy;
    st.x *= u_resolution.x/u_resolution.y;
    vec3 color = vec3(0.0);

    float t = 1.0;
    // Uncomment to animate
     t = abs(1.0-sin(u_time*.1))*2.;
     t -= abs(dot(t, st.y));
    // Comment and uncomment the following lines:
   
    
   
    st += (noise(st*2.*(abs(sin(u_mouse/1000.0)))/2.0)*t)/0.5;
    //st *= noise(st*2.)*t; per il sito  
    st *= noise(radians(st*2.*u_time))*t*radians(t);
  
    color = vec3(1.) * smoothstep(.21,.20,noise(st)); // Big black drops by Onopko
    color -= vec3(1.) * smoothstep(.18,.20,noise(st)); // Big black drops
    color -= smoothstep(.15,.2,noise(st*10.)); // Black splatter
    color += smoothstep(.35,.4,noise(st*10.)); // Holes on splatter
    
    color -= vec3(0.5)*smoothstep(.85,1.0,noise(st/noise(st*u_time))); // Holes on splatter
    color -= vec3(0.5)*smoothstep(.65,.8,noise(st/noise(st*u_time)));
    
    gl_FragColor = vec4(0.0,0.0, 1.-color.x, 1.0);
    //gl_FragColor = vec4(0.1 - vec3(color.x/10.0), 1.0); per il sito
    //gl_FragColor = vec4(0.1 - vec3(color.x- color.y/20.0), 1.0);
    // gl_FragColor = vec4(0.1 - vec3(color.x/sin(u_time)*0.05), 1.0); alternativa sito
}
        `;


  esegui() {
    this.setState({ vertex: this.vertex, fragment: this.fragment })
  }

  mouseMove(e){

    this.setState({
      mouse: {
        x: (e.pageX - window.innerWidth / 2)/2,
        y: (e.pageY - window.innerHeight / 2)/2
      }
    })

  }

  constructor(props) {
    super(props);
    this.esegui = this.esegui.bind(this);
    this.state = {
      width: null,
      shaderActive: true,
      vertex: this.vertex,
      fragment: this.fragment,
      mouse: {},
    }
  }

  componentDidMount() {
    this.sidebar = document.getElementById('sidebar').clientWidth;
    this.renderWidth = window.innerWidth - this.sidebar - 540; // 540 dimensione contenitore
    this.setState({ width: this.renderWidth });
  }

  render() {
    return (
      <section id="scene">
        <GeneralManager width={this.state.width} height={window.innerHeight}>
          <SceneLight {...this.summary}/>
          <MaterialGeometry shaderActive={this.state.shaderActive} vertex={this.state.vertex}
                            fragment={this.state.fragment} />
        </GeneralManager>
      </section>
    )
  }
}

export default TrediciPagina;